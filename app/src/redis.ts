import { createClient } from 'redis';
import { REDIS_CONNECTION_URL } from './config';

export const redis = createClient({
  url: REDIS_CONNECTION_URL,
});
