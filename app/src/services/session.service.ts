import {
  escapeRedis,
  JoinPayload,
  Profile,
  Session,
  undashUUID,
} from '@yggdrasil/common';
import { randomBytes } from 'crypto';
import { SchemaFieldTypes } from 'redis';
import { SESSION_TIMEOUT } from '../config';
import { redis } from '../redis';

const tokenNS = (token: string) => `minecraft:yggdrasil:session:${token}`;

export async function issue(profile: Profile): Promise<string> {
  const key = randomBytes(60).toString('hex');
  const access = Buffer.from(key).toString('base64');

  const session: Session = {
    id: profile.id,
    serverId: null,
    ip: null,
  };

  await redis.json.set(tokenNS(access), '$', session as any);

  await redis.expire(tokenNS(access), SESSION_TIMEOUT);

  return access;
}

export async function getSession(
  access?: string | null,
): Promise<Session | null> {
  if (!access) return null;

  return redis.json.get(tokenNS(access)) as unknown as Session;
}

export async function exists(access: string, uuid: string): Promise<boolean> {
  const session = await getSession(access);

  console.debug(session);

  return undashUUID(session?.id!) === undashUUID(uuid);
}

export async function join(
  payload: JoinPayload,
  ip: string | null,
): Promise<void> {
  const session: Session = {
    id: payload.selectedProfile,
    serverId: escapeRedis(payload.serverId),
    ip,
  };

  await redis.json.set(tokenNS(payload.accessToken), '$', session as any);
}

export async function hasJoined(
  serverId: string,
  id: string,
  ip?: string,
): Promise<boolean> {
  return redis.ft
    .search(
      'idx:session',
      `@serverId:"${escapeRedis(serverId)}" @id:"${id}" ${
        ip ? `@ip:"${ip}"` : ''
      }`,
      { LIMIT: { from: 0, size: 1 } },
    )
    .then((res) => res.total > 0);
}

export async function index() {
  await redis.ft
    .create(
      'idx:session',
      {
        '$.serverId': { AS: 'serverId', type: SchemaFieldTypes.TEXT },
        '$.id': { AS: 'id', type: SchemaFieldTypes.TEXT },
        '$.ip': { AS: 'ip', type: SchemaFieldTypes.TEXT },
      },
      { ON: 'JSON' },
    )
    .then(() => console.log('Redis idx:session created'))
    .catch((e) => e.message === 'Index already exists' || console.error(e));
}
