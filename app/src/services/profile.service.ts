import {
  escapeRedis,
  Profile,
  UA_FLAG_CAPE,
  undashUUID,
} from '@yggdrasil/common';
import { SchemaFieldTypes } from 'redis';
import { DISABLE_UA_INDEPENDENT_DAY_FEATURE } from '../config';
import { redis } from '../redis';

const profileNS = (uuid: string) =>
  `minecraft:yggdrasil:profile:${undashUUID(uuid)}`;

async function profileInterceptor<T extends Profile>(profile: T): Promise<T> {
  if (isUaIndependentDay()) {
    if (!profile.textures) {
      profile.textures = {};
    }

    profile.textures.cape = UA_FLAG_CAPE;
  }

  return profile;
}

export async function upsert(
  uuid: string,
  data: Omit<Profile, 'id'>,
): Promise<Profile> {
  const profile: Profile = { id: undashUUID(uuid), ...data };
  await redis.json.set(profileNS(uuid), '$', profile as any);

  return profileInterceptor(profile);
}

export async function getByUuid(uuid: string): Promise<Profile | null> {
  const profile = (await redis.json.get(profileNS(uuid))) as unknown as Profile;

  if (!profile) return null;

  return profileInterceptor(profile);
}

export async function getByName(name: string): Promise<Profile | null> {
  const profile = await redis.ft
    .search('idx:profile', `@name:${name}`)
    .then((res) => res.documents.at(0)?.value as unknown as Profile)
    .catch(() => null);

  if (!profile) return null;

  return profileInterceptor(profile);
}

export async function lookupByNames(
  names: string[],
): Promise<{ id: string; name: string }[]> {
  return redis.ft
    .search('idx:profile', `@name:(${names.map(escapeRedis).join('|')})`, {
      RETURN: ['id', 'name'],
    })
    .then<Profile[]>((res) => res.documents.map<any>((doc) => doc.value))
    .catch(() => []);
}

export async function index() {
  await redis.ft
    .create(
      'idx:profile',
      {
        '$.name': { AS: 'name', type: SchemaFieldTypes.TEXT },
        '$.id': { AS: 'id', type: SchemaFieldTypes.TEXT },
      },
      { ON: 'JSON' },
    )
    .then(() => console.log('Redis idx:profile created'))
    .catch((e) => e.message === 'Index already exists' || console.error(e));
}

function isUaIndependentDay() {
  const date = new Date();
  return (
    !DISABLE_UA_INDEPENDENT_DAY_FEATURE &&
    date.getDate() === 24 &&
    date.getMonth() + 1 === 8
  );
}
