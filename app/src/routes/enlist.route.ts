import { zValidator } from '@hono/zod-validator';
import { header } from '@yggdrasil/common';
import { Hono } from 'hono';
import { z } from 'zod';
import { SECRET_KEY } from '../config';
import { profiles, sessions } from '../services';

export const enlist = new Hono();

const EnlistPayload = z.object({
  name: z.string().regex(/^[0-9A-Z_]{1,16}$/i),
  textures: z
    .object({
      skin: z.string().url().nullable().optional(),
      cape: z.string().url().nullable().optional(),
      model: z.enum(['slim', 'classic']).nullable().optional(),
      signature: z.string().nullable().optional(),
    })
    .optional(),
});

const UUID = z
  .string()
  .uuid()
  // uuid without dashes
  .or(z.string().regex(/^[0-9A-F]{32}$/i));

enlist.on(
  ['POST', 'PUT'],
  '/enlist/:uuid',
  header(
    'X-FMA-Truth-Gateway',
    'Humankind cannot gain anything without first giving something in return.',
  ),
  zValidator(
    'header',
    z.object({ authorization: z.literal(SECRET_KEY) }),
    (result: any, c) => result?.error && c.notFound(),
  ),
  zValidator('param', z.object({ uuid: UUID })),
  zValidator('json', EnlistPayload),
  async (c) => {
    const uuid = c.req.param('uuid');
    const data = await c.req.json<z.infer<typeof EnlistPayload>>();

    const profile = await profiles.upsert(uuid, data);

    if (c.req.method === 'PUT') {
      return c.body(null, 204);
    }

    const access = await sessions.issue(profile);
    return c.json({ access }, 200);
  },
);
