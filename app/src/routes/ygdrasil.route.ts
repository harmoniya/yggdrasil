import { getConnInfo } from '@hono/node-server/conninfo';
import {
  DEFAULT_PRIVILEGES,
  JoinPayload,
  profileToResponse,
} from '@yggdrasil/common';
import { Context, Hono } from 'hono';
import { profiles, sessions } from '../services';

export const yggdrasil = new Hono();

const forbidden = (c: Context) =>
  c.json({ error: 'Forbidden', path: c.req.path }, 403);

const unauthorized = (c: Context) => c.json({ path: c.req.path }, 401);

/** @see https://wiki.vg/Protocol_Encryption#Server */
yggdrasil.get('/session/minecraft/hasJoined', async (c) => {
  const { serverId, username, ip } = c.req.query();

  console.debug({ serverId, username, ip });

  const profile = await profiles.getByName(username);

  if (!profile) return forbidden(c);

  console.debug(profile);

  const exists = await sessions.hasJoined(serverId, profile.id, ip);

  if (!exists) return forbidden(c);

  return c.json(profileToResponse(profile));
});

/** @see https://wiki.vg/Protocol_Encryption#Client */
yggdrasil.post('/session/minecraft/join', async (c) => {
  const body = await c.req.json<JoinPayload>();
  const ip = getConnInfo(c).remote.address!;

  console.debug(ip, body);

  const exists = await sessions.exists(body.accessToken, body.selectedProfile);

  if (!exists) return forbidden(c);

  await sessions.join(body, ip ?? null);

  return c.body(null, 204);
});

/** @see https://wiki.vg/Mojang_API#UUID_to_Profile_and_Skin.2FCape */
yggdrasil.get('/session/minecraft/profile/:uuid', async (c) => {
  const uuid = c.req.param('uuid');
  const profile = await profiles.getByUuid(uuid);

  if (!profile) {
    console.debug('Profile is not found.');
    return c.body(null, 204);
  }

  console.debug(profile);
  return c.json(profileToResponse(profile));
});

/** @see https://wiki.vg/Mojang_API#Usernames_to_UUIDs */
yggdrasil.on(
  'POST',
  ['/profiles/minecraft', 'minecraft/profile/lookup/bulk/byname'],
  async (c) =>
    c.json(await profiles.lookupByNames(await c.req.json<string[]>())),
);

/** @see https://wiki.vg/Mojang_API#Player_Attributes */
yggdrasil.on(
  'GET',
  ['/privileges', '/player/attributes'],
  async (c, next) => {
    const token = c.req
      .header('Authorization')
      ?.replace?.('Bearer', '')
      ?.trim();

    const session = await sessions.getSession(token);
    if (!session) return unauthorized(c);

    await next();
  },
  (c) => c.json(DEFAULT_PRIVILEGES),
);
/** @see https://wiki.vg/Mojang_API#Player_Blocklist */
yggdrasil.get('/privacy/blocklist', (c) => c.json({ blockedProfiles: [] }));
