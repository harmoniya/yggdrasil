import ms, { StringValue } from 'ms';

if (process.env.SESSION_TIMEOUT && !ms(process.env.SESSION_TIMEOUT as any)) {
  console.warn(
    '[SESSION_TIMEOUT]',
    process.env.SESSION_TIMEOUT,
    'is invalid value!',
  );
}

export const SESSION_TIMEOUT =
  process.env.SESSION_TIMEOUT && ms(process.env.SESSION_TIMEOUT as any)
    ? ms(process.env.SESSION_TIMEOUT as StringValue)
    : ms('12h');

console.log('Session timeout is set to', ms(SESSION_TIMEOUT));

export const DISABLE_UA_INDEPENDENT_DAY_FEATURE =
  process.env.DISABLE_INDEPENDENT_DAY_FEATURE === 'true';

export const PORT = parseInt(process.env.PORT!) || 3000;

export const REDIS_CONNECTION_URL = process.env.REDIS_CONNECTION_URL!;
export const SECRET_KEY = process.env.SECRET_KEY! ?? null;
