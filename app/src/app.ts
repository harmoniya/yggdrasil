import { serve } from '@hono/node-server';
import { header } from '@yggdrasil/common';
import { Hono } from 'hono';
import { showRoutes } from 'hono/dev';
import { logger } from 'hono/logger';
import { PORT } from './config';
import { redis } from './redis';
import { enlist, yggdrasil } from './routes';
import { profiles, sessions } from './services';

const app = new Hono();

app.use(header('x-powered-by', 'Harmoniya Yggdrasil server'));

app.use(logger());
app.route('/', yggdrasil);
app.route('/', enlist);
showRoutes(app);

async function main() {
  await redis.connect();

  await profiles.index();
  await sessions.index();

  serve({
    fetch: app.fetch,
    port: PORT,
  });

  console.log(`Yggdrasil server is running on port ${PORT}`);
}

main();
