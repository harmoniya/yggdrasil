import { serve } from '@hono/node-server';
import { DEFAULT_PRIVILEGES, profileToResponse } from '@yggdrasil/common';
import { Hono } from 'hono';
import { showRoutes } from 'hono/dev';
import { logger } from 'hono/logger';
import { profiles } from './profiles';

const app = new Hono();
app.use(logger());

// this profiles will be loaded from mojang api
profiles.loadMojang('DeityLamb');
profiles.loadMojang('OxyOxx');
profiles.loadMojang('Notch');

// you can add more your own profiles here
profiles.load({
  name: 'Harmoniya',
  id: '00000000-0000-0000-0000-000000000000',
  textures: {
    skin: 'http://textures.minecraft.net/texture/292009a4925b58f02c77dadc3ecef07ea4c7472f64e0fdc32ce5522489362680',
  },
});

app.post('/session/minecraft/join', async (c) => c.body(null, 204));
app.get('/session/minecraft/hasJoined', async (c) => {
  const profile = profiles.byName(c.req.query('username')!);
  return profile ? c.json(profileToResponse(profile)) : c.body(null, 403);
});

app.get('/session/minecraft/profile/:uuid', (c) => {
  const profile = profiles.byUuid(c.req.param('uuid'));
  return profile ? c.json(profileToResponse(profile)) : c.body(null, 204);
});

app.get('/privileges', (c) => c.json(DEFAULT_PRIVILEGES));
app.get('/privacy/blocklist', (c) => c.json({ blockedProfiles: [] }));

const routes = ['/minecraft/profile/lookup/bulk/byname', '/profiles/minecraft'];
app.on('POST', routes, (c) => c.json(profiles.names()));

const port = parseInt(process.env.PORT!) || 4000;

showRoutes(app);
console.log(`Dummy yggdrasil server is running on port ${port}`);
serve({ fetch: app.fetch, port });
