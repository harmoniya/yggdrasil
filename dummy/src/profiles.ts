import { Profile, undashUUID } from '@yggdrasil/common';

class ProfileStore {
  public profiles: Profile[] = [];

  public byName(name: string) {
    return this.profiles.find((p) => p.name === name);
  }

  public byUuid(uuid: string) {
    return this.profiles.find((p) => undashUUID(p.id) === undashUUID(uuid));
  }

  public names() {
    return this.profiles.map(({ name, id }) => ({ name, id }));
  }

  public load(profile: Profile) {
    this.profiles.push(profile);
  }

  async loadMojang(name: string) {
    const user = await fetch(
      `https://api.mojang.com/users/profiles/minecraft/${name}`,
    ).then((res) => res.json());

    if (!user.id) return null;

    const profile = await fetch(
      `https://sessionserver.mojang.com/session/minecraft/profile/${user.id}?unsigned=false`,
    ).then((res) => res.json());

    const property = profile.properties.find((p: any) => p.name === 'textures');

    if (!property?.value) return null;

    const { textures } = JSON.parse(
      Buffer.from(property.value, 'base64').toString(),
    );

    this.load({
      name: user.name,
      id: user.id,
      textures: {
        skin: textures?.SKIN?.url,
        model: textures?.SKIN?.model,
        cape: textures?.CAPE?.url,
        signature: property?.signature,
      },
    });

    console.log('Loaded mojang profile', name);
  }
}

export const profiles = new ProfileStore();
