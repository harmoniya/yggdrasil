FROM node:20-alpine AS build

WORKDIR /build

COPY ./ ./

RUN npm install
RUN npm run app:build

FROM node:20-alpine

WORKDIR /app

COPY package*.json ./

COPY --from=build /build/common/dist ./common/dist
COPY --from=build /build/common/package.json ./common/

COPY --from=build /build/app/dist ./app/dist
COPY --from=build /build/app/package.json ./app/

RUN npm ci -w app

CMD [ "npm", "run", "app" ]
