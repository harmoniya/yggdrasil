export const DEFAULT_PRIVILEGES = {
  privileges: {
    onlineChat: { enabled: true },
    multiplayerServer: { enabled: true },
    multiplayerRealms: { enabled: false },
    telemetry: { enabled: false },
  },
  profanityFilterPreferences: { profanityFilterOn: false },
};

export const UA_FLAG_CAPE =
  'https://raw.githubusercontent.com/harmoniamc/launcher-releases/main/ua.png#c8ac9a73f263014ecb1021b743bea10d6cc79af5fd0e6aeabeac259f5ce8e7a9';
