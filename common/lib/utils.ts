import { Profile } from './types';

export function undashUUID(uuid: string) {
  return uuid?.replace(/-/g, '').toLowerCase();
}

export function escapeRedis(str: string) {
  return str.replaceAll('-', '\\-');
}

const profileToBlob = (profile: Profile) => ({
  timestamp: Date.now(),
  profileId: undashUUID(profile.id),
  profileName: profile.name,
  signatureRequired: false,
  textures: {
    ...(profile?.textures?.skin && {
      SKIN: {
        url: profile?.textures?.skin,
        metadata: {
          model: profile?.textures?.model || 'classic',
        },
      },
    }),
    ...(profile?.textures?.cape && {
      CAPE: { url: profile?.textures?.cape },
    }),
  },
});

export const profileToResponse = (profile: Profile) => ({
  id: undashUUID(profile.id),
  name: profile.name,
  properties: [
    {
      name: 'textures',
      value: Buffer.from(
        JSON.stringify(profileToBlob(profile)),
        'utf-8',
      ).toString('base64'),
      signature: profile?.textures?.signature ?? '',
    },
  ],
  profileActions: [],
});

export const header = (key: string, value: string) => (c: any, next: any) => {
  c.header(key, value);
  return next();
};
