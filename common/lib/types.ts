export interface JoinPayload {
  accessToken: string;
  selectedProfile: string;
  serverId: string;
}

export interface Profile {
  id: string;
  name: string;
  textures?: {
    skin?: string | null;
    cape?: string | null;
    model?: string | null;
    signature?: string | null;
  };
}

export interface Session {
  id: string;
  serverId: string | null;
  ip: string | null;
}
